﻿namespace BookStore.Application.DTOs.AccountBalanceDTOs
{
    public class AccountBalanceForGetDTO
    {
        public Guid Id { get; set; }
        public decimal Balance { get; set; }
    }
}
