﻿using BookStore.Application.DTOs.BookDTOs;
using BookStore.Domain.Commons.Enums;

namespace BookStore.Application.DTOs.ReviewDTOs
{
    public class ReviewForGetDTO
    {
        public Guid Id { get; set; }
        public Guid CustomerId { get; set; }
        public Guid BookId { get; set; }
        public string Comment { get; set; } = string.Empty;
        public RateEnum Rate { get; set; }
        public string ImgUrl { get; set; } = string.Empty;
        public BookForGetDTO? Book { get; set; }
    }
}