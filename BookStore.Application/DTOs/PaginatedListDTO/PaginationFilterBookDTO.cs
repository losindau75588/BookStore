﻿using BookStore.Application.Commons.DataAnnotations;
using System.ComponentModel.DataAnnotations;

namespace BookStore.Application.DTOs.PaginatedListDTO
{
    public class PaginationFilterBookDTO
    {
        [Required]
        public int PageNumber { get; set; }

        [Required]
        public int PageSize { get; set; }

        public string? Title { get; set; }

        public string? Author { get; set; }

        public string? Genre { get; set; }

        [DateLessThanCurrentDate]
        public DateTime? PublicationDate { get; set; }
    }
}
