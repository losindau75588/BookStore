﻿namespace BookStore.Application.DTOs.UserDTOs
{
    public class UserForReponseDTO
    {
        public Guid Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public string Email { get; set; } = string.Empty;
        public string Role { get; set; } = string.Empty;
    }
}