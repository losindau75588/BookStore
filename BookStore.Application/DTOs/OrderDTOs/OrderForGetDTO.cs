﻿using BookStore.Application.DTOs.CustomerDTOs;
using BookStore.Application.DTOs.OrderDetailDTOs;

namespace BookStore.Application.DTOs.OrderDTOs
{
    public class OrderForGetDTO
    {
        public Guid Id { get; set; }
        public CustomerForGetDTO? Customer { get; set; }
        public DateTime OrderDate { get; set; }
        public DateTime PurchaseDate { get; set; }
        public bool IsPurchase { get; set; }
        public decimal Total { get; set; }
        public ICollection<OrderDetailForGetDTO> OrderDetails { get; set; } =  new List<OrderDetailForGetDTO>();
    }
}