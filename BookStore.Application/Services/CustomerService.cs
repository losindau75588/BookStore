﻿using BookStore.Application.Commons.Constants;
using BookStore.Application.DTOs.CustomerDTOs;
using BookStore.Application.Interfaces;
using BookStore.Domain.Interfaces;
using MapsterMapper;

namespace BookStore.Application.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CustomerService(IUnitOfWork unitOfWor, IMapper mapper)
        {
            _unitOfWork = unitOfWor;
            _mapper = mapper;
        }

        public async Task<CustomerForGetDTO> GetByIdAsync(Guid id)
        {
            var customer = await _unitOfWork.CustomerRepository.GetByIdAsync(id);
            var customerForGetDTO = _mapper.Map<CustomerForGetDTO>(customer!);

            return customerForGetDTO;
        }
    }
}
