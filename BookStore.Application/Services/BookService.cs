﻿using BookStore.Application.DTOs.BookDTOs;
using BookStore.Application.DTOs.PaginatedListDTO;
using BookStore.Application.Interfaces;
using BookStore.Domain.Commons;
using BookStore.Domain.Entities;
using BookStore.Domain.Interfaces;
using MapsterMapper;

namespace BookStore.Application.Services
{
    public class BookService : IBookService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public BookService(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task CreateBookAsync(BookForUpsertDTO bookForUpsertDTO)
        {
            var book = new Book(bookForUpsertDTO.Title,
                bookForUpsertDTO.Author,
                bookForUpsertDTO.Genre,
                bookForUpsertDTO.PublicationDate,
                bookForUpsertDTO.CoverImgUrl,
                bookForUpsertDTO.Quantity,
                bookForUpsertDTO.UnitPrice);

            await _unitOfWork.BookRepository.AddAsync(book);

            await _unitOfWork.SaveChangesAsync();   
        }

        public async Task UpdateBookAsync(Guid bookId, BookForUpsertDTO bookForUpsertDTO)
        {
            var book = await _unitOfWork.BookRepository.GetByIdAsync(bookId);

            book!.UpdateDetails(
                bookForUpsertDTO.Title,
                bookForUpsertDTO.Author,
                bookForUpsertDTO.Genre,
                bookForUpsertDTO.PublicationDate,
                bookForUpsertDTO.CoverImgUrl,
                bookForUpsertDTO.Quantity,
                bookForUpsertDTO.UnitPrice);

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task DeleteBookAsync(Guid bookId)
        {
            var book = await _unitOfWork.BookRepository.GetByIdAsync(bookId);
            book!.IsDelete = true;

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<PaginatedList<BookForGetDTO>> FilterBooksAsync(PaginationFilterBookDTO paginatedListFilterBookDTO)
        {
            var paginatedListBook = await _unitOfWork.BookRepository.FilterBooksAsync(
                paginatedListFilterBookDTO.PageNumber,
                paginatedListFilterBookDTO.PageSize,
                paginatedListFilterBookDTO.Title,
                paginatedListFilterBookDTO.Author,
                paginatedListFilterBookDTO.Genre,
                paginatedListFilterBookDTO.PublicationDate);

            var paginatedListBookForGetDTO = _mapper.Map<PaginatedList<BookForGetDTO>>(paginatedListBook);

            return paginatedListBookForGetDTO;
        }

        public async Task<PaginatedList<BookForGetDTO>> GetAllAsync(PaginationDTO paginatedListDTO)
        {
            var paginatedListBook = await _unitOfWork.BookRepository.GetAllAsync(paginatedListDTO.PageNumber, paginatedListDTO.PageSize);
            var paginatedListBookForGetDTO = _mapper.Map<PaginatedList<BookForGetDTO>>(paginatedListBook);

            return paginatedListBookForGetDTO;
        }
    }
}