﻿using BookStore.Application.DTOs.FavoriteBookDTOs;
using BookStore.Application.DTOs.PaginatedListDTO;
using BookStore.Domain.Commons;

namespace BookStore.Application.Interfaces
{
    public interface IFavoriteBookService
    {
        Task CreateFavoriteBook(FavoriteBookForInsertDTO favoriteBookForUpsertDTO);
        Task DeleteFavoriteBook(Guid favoriteBookId);
        Task<PaginatedList<FavoriteBookForGetDTO>> GetByCustomerIdAsync(Guid customerId, PaginationDTO paginationDTO);
    }
}