﻿using BookStore.Application.DTOs.CustomerDTOs;

namespace BookStore.Application.Interfaces
{
    public interface ICustomerService
    {
        Task<CustomerForGetDTO> GetByIdAsync(Guid id);
    }
}
