﻿namespace BookStore.Application.Interfaces
{
    public interface IReportGenerator
    {
        Task<byte[]> GenerateReportAsync();
    }
}
