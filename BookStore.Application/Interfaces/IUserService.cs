﻿using BookStore.Application.DTOs.CustomerDTOs;
using BookStore.Application.DTOs.OtpCodeDTOs;
using BookStore.Application.DTOs.UserDTOs;

namespace BookStore.Application.Interfaces
{
    public interface IUserService
    {
        public Task<string> SignInAsync(UserForRequestDTO userForRequestDTO);
        public Task SignUpAsync(CustomerForInsertDTO customerForInsertDTO);
        Task ForGotPasswordAsync(string email);
        Task ResetPasswordAsync(OtpCodeForRequest otpCodeForRequest);
    }
}