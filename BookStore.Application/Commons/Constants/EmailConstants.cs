﻿namespace BookStore.Application.Commons.Constants
{
    public static class EmailConstants
    {
        public const string CheckOutOrderSubject = "Congratulation! You have checked out your order successfully!";
    }
}
