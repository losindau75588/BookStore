﻿using BookStore.Application.DTOs.AccountBalanceDTOs;
using BookStore.Domain.Entities;
using Mapster;

namespace BookStore.Application.Commons.Mapping
{
    public class AccountBalanceMappingConfig : IRegister
    {
        public void Register(TypeAdapterConfig config)
        {
            config.NewConfig<AccountBalance, AccountBalanceForGetDTO>();
        }
    }
}
