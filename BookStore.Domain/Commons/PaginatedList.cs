﻿
namespace BookStore.Domain.Commons
{
    public class PaginatedList<T>
    {
        public int PageNumber { get; set; }
        public int TotalPages { get; set; }
        public List<T> Items { get; set; } = new();

        public PaginatedList()
        {

        }

        public PaginatedList(List<T> items, int count, int pageNumber, int pageSize)
        {
            PageNumber = pageNumber;
            TotalPages = (int)Math.Ceiling(count / (double)pageSize);
            Items = items;
        }

        public bool HasPreviousPage
        {
            get
            {
                return (PageNumber > 1);
            }
        }

        public bool HasNextPage
        {
            get
            {
                return (PageNumber < TotalPages);
            }
        }
    }
}