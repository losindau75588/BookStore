﻿using BookStore.Domain.Commons;
using BookStore.Domain.Entities;

namespace BookStore.Domain.Interfaces
{
    public interface IFavoriteBookRepository : IRepository<FavoriteBook>
    {
        Task<PaginatedList<FavoriteBook>> GetByCustomerIdAsync(Guid customerId, int pageNumber, int pageSize);
    }
}
