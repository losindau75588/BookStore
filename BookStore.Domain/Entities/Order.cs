﻿using BookStore.Domain.Commons.Constants;

namespace BookStore.Domain.Entities
{
    public class Order
    {
        private Guid _id = Guid.NewGuid();
        private Guid _customerId;
        private DateTime _orderDate;
        private DateTime _purchaseDate;
        private bool _isPurchase;
        private decimal _total;

        private Order() { }

        public Order(Guid customerId, DateTime orderDate)
        {
            CustomerId = customerId;
            OrderDate = orderDate;
        }

        public Guid Id => _id;

        public Guid CustomerId { get => _customerId; private set => _customerId = value; }

        public DateTime OrderDate
        {
            get { return _orderDate; }
            private set
            {
                if (value.Date != DateTime.UtcNow.Date)
                {
                    throw new ArgumentOutOfRangeException(ErrorMessages.DateMustEqualCurrentDate);
                }

                _orderDate = value;
            }
        }

        public DateTime PurchaseDate
        {
            get { return _purchaseDate; }
            private set
            {
                if (value.Date > DateTime.UtcNow.Date)
                {
                    throw new ArgumentOutOfRangeException(ErrorMessages.DateCannotBeLaterThanCurrentDate);
                }

                if (value < _orderDate)
                {
                    throw new ArgumentOutOfRangeException(ErrorMessages.PurchaseDateCannotBeEarlierThanOrderDate);
                }

                _purchaseDate = value;
            }
        }

        public bool IsPurchase { get => _isPurchase; private set => _isPurchase = value; }

        public decimal Total
        {
            get { return _total; }
            private set
            {
                if (value < 0)
                {
                    throw new ArgumentOutOfRangeException(ErrorMessages.NumberMustBePositive);
                }

                _total = value;
            }
        }

        public bool IsDelete { get; set; }

        public Customer? Customer { get; set; }

        public ICollection<OrderDetail> OrderDetails { get; set; } = new List<OrderDetail>();
        
        public void CheckOut()
        {
            PurchaseDate = DateTime.UtcNow;
            IsPurchase = true;
        }

        public void CalculateTotal(decimal unitPrice, int quantity)
        {
            if (unitPrice < 0 || quantity < 0)
            {
                throw new ArgumentOutOfRangeException(ErrorMessages.NumberMustBePositive);
            }

            Total += unitPrice * quantity;
        }
    }
}