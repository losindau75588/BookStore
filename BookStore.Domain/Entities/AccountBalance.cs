﻿using BookStore.Domain.Commons.Constants;

namespace BookStore.Domain.Entities
{
    public class AccountBalance
    {
        private Guid _id = Guid.NewGuid();
        private decimal _balance;

        public AccountBalance() { }

        public AccountBalance(decimal balance)
        {
            Balance = balance;
        }

        public Guid Id => _id;

        public decimal Balance
        {
            get { return _balance; }
            private set
            {
                if (value < 0)
                {
                    throw new ArgumentOutOfRangeException(ErrorMessages.NumberMustBePositive);
                }

                _balance = value;
            }
        }

        public bool IsDelete { get; set; }

        public Customer? Customer { get; set; }

        public void Deposit(decimal amount)
        {
            if (amount < 0)
            {
                throw new ArgumentOutOfRangeException(ErrorMessages.NumberMustBePositive);
            }

            Balance += amount;
        }

        public void CheckOut(decimal amount)
        {
            if (amount < 0)
            {
                throw new ArgumentOutOfRangeException(ErrorMessages.NumberMustBePositive);
            }

            if (Balance < amount)
            {
                throw new Exception(ErrorMessages.InsufficientBalance);
            }

            Balance -= amount;
        }
    }
}