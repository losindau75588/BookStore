﻿using BookStore.Domain.Commons.Constants;
using BookStore.Domain.Commons.Enums;

namespace BookStore.Domain.Entities
{
    public class Review
    {
        private Guid _id = Guid.NewGuid();
        private Guid _customerId;
        private Guid _bookId;
        private string _comment = string.Empty;
        private RateEnum _rate;
        private string _imgUrl = string.Empty;
        private bool _isDeleted;

        private Review() { }

        public Review(Guid customerId, Guid bookId, string comment, RateEnum rate, string imgUrl)
        {
            CustomerId = customerId;
            BookId = bookId;
            Comment = comment;
            Rate = rate;
            ImgUrl = imgUrl;
        }

        public Guid Id => _id;

        public Guid CustomerId { get => _customerId; private set => _customerId = value; }

        public Guid BookId { get => _bookId; private set => _bookId = value; }

        public string Comment
        {
            get { return _comment; }
            private set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentException(ErrorMessages.StringCannotBeEmpty);
                }

                _comment = value;
            }
        }

        public RateEnum Rate
        {
            get { return _rate; }
            private set
            {
                if ((int)value > 2 || (int)value < 0)
                {
                    throw new ArgumentOutOfRangeException(ErrorMessages.InvalidEnumRate);
                }

                _rate = value;
            }
        }

        public string ImgUrl
        {
            get { return _imgUrl; }
            private set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentException(ErrorMessages.StringCannotBeEmpty);
                }

                _imgUrl = value;
            }
        }

        public bool IsDeleted { get => _isDeleted; set => _isDeleted = value; }

        public Customer? Customer { get; set; }

        public Book? Book { get; set; }
    }
}