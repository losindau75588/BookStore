﻿namespace BookStore.Domain.Entities
{
    public class FavoriteBook
    {
        private Guid _id = Guid.NewGuid();
        private Guid _customerId;
        private Guid _bookId;
        private bool _isDelete;

        private FavoriteBook() { }

        public FavoriteBook(Guid customerId, Guid bookId)
        {
            _customerId = customerId;
            _bookId = bookId;
        }

        public Guid Id => _id;

        public Guid CustomerId { get => _customerId; private set => _customerId = value; }

        public Guid BookId { get => _bookId; private set => _bookId = value; }

        public bool IsDelete { get => _isDelete; set => _isDelete = value; }

        public Customer? Customer { get; set; }

        public Book? Book { get; set; }
    }
}