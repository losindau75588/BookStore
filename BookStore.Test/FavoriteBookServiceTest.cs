﻿using BookStore.Application.DTOs.FavoriteBookDTOs;
using BookStore.Application.DTOs.PaginatedListDTO;
using BookStore.Application.Services;
using BookStore.Domain.Commons;
using BookStore.Domain.Entities;
using BookStore.Domain.Interfaces;
using MapsterMapper;
using Moq;

namespace BookStore.Test
{
    public class FavoriteBookServiceTest
    {
        private readonly Mock<IMapper> _mapperMock;
        private readonly Mock<IUnitOfWork> _unitOfWorkMock;
        private readonly FavoriteBookService _favoriteBookService;

        public FavoriteBookServiceTest()
        {
            _mapperMock = new Mock<IMapper>();
            _unitOfWorkMock = new Mock<IUnitOfWork>();
            _favoriteBookService = new FavoriteBookService(_mapperMock.Object, _unitOfWorkMock.Object);
        }

        [Fact]
        public async Task CreateFavoriteBook_ShouldCallSaveChangesAsync()
        {
            // Arrange
            var favoriteBookForInsertDTO = new FavoriteBookForInsertDTO()
            {
                CustomerId = Guid.NewGuid(),
                BookId = Guid.NewGuid()
            };

            _unitOfWorkMock.Setup(uow => uow.FavoriteBookRepository.AddAsync(It.IsAny<FavoriteBook>())).Returns(Task.CompletedTask);

            // Act
            await _favoriteBookService.CreateFavoriteBook(favoriteBookForInsertDTO);

            // Assert
            _unitOfWorkMock.Verify(uow => uow.FavoriteBookRepository.AddAsync(It.IsAny<FavoriteBook>()), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.SaveChangesAsync(), Times.Once);
        }

        [Fact]
        public async Task DeleteFavoriteBook_ShouldCallSaveChangesAsync()
        {
            // Arrange
            var existingFavoriteBook = new FavoriteBook(It.IsAny<Guid>(), It.IsAny<Guid>());
            _unitOfWorkMock.Setup(uow => uow.FavoriteBookRepository.GetByIdAsync(existingFavoriteBook.Id)).ReturnsAsync(existingFavoriteBook);

            // Act
            await _favoriteBookService.DeleteFavoriteBook(existingFavoriteBook.Id);

            // Assert
            Assert.True(existingFavoriteBook.IsDelete);

            _unitOfWorkMock.Verify(uow => uow.FavoriteBookRepository.GetByIdAsync(It.IsAny<Guid>()), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.SaveChangesAsync(), Times.Once);
        }

        [Fact]
        public async Task GetByCustomerIdAsync_ShouldCallGetByCustomerIdAsync()
        {
            // Arrange
            var paginationDTO = new PaginationDTO
            {
                PageNumber = 1,
                PageSize = 10
            };

            var favoriteBooks = new List<FavoriteBook>()
            {
                new FavoriteBook(It.IsAny<Guid>(), It.IsAny<Guid>()),
                new FavoriteBook(It.IsAny<Guid>(), It.IsAny<Guid>())
            };

            var favoriteBooksForGetDTO = new List<FavoriteBookForGetDTO>()
            {
                new FavoriteBookForGetDTO(),
                new FavoriteBookForGetDTO()
            };

            var paginatedListFavoriteBook = new PaginatedList<FavoriteBook>(
                favoriteBooks,
                favoriteBooks.Count,
                paginationDTO.PageNumber,
                paginationDTO.PageSize);

            _unitOfWorkMock.Setup(
                uow => uow.FavoriteBookRepository.GetByCustomerIdAsync(It.IsAny<Guid>(),
                paginationDTO.PageNumber,
                paginationDTO.PageSize)).ReturnsAsync(paginatedListFavoriteBook);

            var mapperResult = new PaginatedList<FavoriteBookForGetDTO>(
                favoriteBooksForGetDTO,
                favoriteBooksForGetDTO.Count,
                paginationDTO.PageNumber,
                paginationDTO.PageSize);

            _mapperMock.Setup(m => m.Map<PaginatedList<FavoriteBookForGetDTO>>(paginatedListFavoriteBook)).Returns(mapperResult);

            // Act
            await _favoriteBookService.GetByCustomerIdAsync(It.IsAny<Guid>(), paginationDTO);

            //Assert
            _unitOfWorkMock.Verify(uow => uow.FavoriteBookRepository.GetByCustomerIdAsync(It.IsAny<Guid>(), It.IsAny<int>(), It.IsAny<int>()));
        }
    }
}
