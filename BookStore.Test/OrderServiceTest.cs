﻿using BookStore.Application.Commons.Constants;
using BookStore.Application.DTOs.BookDTOs;
using BookStore.Application.DTOs.FavoriteBookDTOs;
using BookStore.Application.DTOs.OrderDetailDTOs;
using BookStore.Application.DTOs.OrderDTOs;
using BookStore.Application.DTOs.PaginatedListDTO;
using BookStore.Application.Interfaces;
using BookStore.Application.Services;
using BookStore.Domain.Commons;
using BookStore.Domain.Entities;
using BookStore.Domain.Interfaces;
using MapsterMapper;
using Moq;
using System.Text;

namespace BookStore.Test
{
    public class OrderServiceTest
    {
        private readonly Mock<IMapper> _mapperMock;
        private readonly Mock<IUnitOfWork> _unitOfWorkMock;
        private readonly Mock<IEmailSender> _emailSender;
        private readonly OrderService _orderService;

        public OrderServiceTest()
        {
            _mapperMock = new Mock<IMapper>();
            _unitOfWorkMock = new Mock<IUnitOfWork>();
            _emailSender = new Mock<IEmailSender>();
            _orderService = new OrderService(_mapperMock.Object, _emailSender.Object, _unitOfWorkMock.Object);
        }

        [Fact]
        public async Task CreateOrderAsync_ShouldCallSaveChangesAsyncAndUpdateBookQuantity()
        {
            // Arrange
            var accountBalance = new AccountBalance(10000000);
            var customer = new Customer(Guid.NewGuid(), accountBalance.Id, "Customer", DateTime.UtcNow, "CustomerEmail");
            customer.AccountBalance = accountBalance;

            _unitOfWorkMock.Setup(uow => uow.CustomerRepository.GetByIdAsync(customer.Id)).ReturnsAsync(customer);

            var books = new List<Book>
            {
                new Book("Book 1", "Author 1", "Genre 1", DateTime.Now, "cover1.jpg", 10, 2),
                new Book("Book 2", "Author 2", "Genre 2", DateTime.Now, "cover2.jpg", 10, 5)
            };

            foreach (var book in books)
            {
                _unitOfWorkMock.Setup(uow => uow.BookRepository.GetByIdAsync(book.Id)).ReturnsAsync(book);
            }

            var orderDetailsForInsertDTO = new List<OrderDetailForInsertDTO>
                {
                    new OrderDetailForInsertDTO
                    {
                        BookId = books[0].Id,
                        UnitPrice = books[0].UnitPrice,
                        Quantity = 5
                    },
                    new OrderDetailForInsertDTO
                    {
                        BookId = books[1].Id,
                        UnitPrice = books[1].UnitPrice,
                        Quantity = 5
                    },
            };

            var orderForInsertDTO = new OrderForInsertDTO
            {
                CustomerId = customer.Id,
                OrderDetails = orderDetailsForInsertDTO
            };

            _unitOfWorkMock.Setup(uow => uow.OrderRepository.AddAsync(It.IsAny<Order>())).Returns(Task.CompletedTask);

            // Act
            await _orderService.CreateOrderAsync(orderForInsertDTO);

            // Assert
            Assert.Equal(5, books[0].Quantity);
            Assert.Equal(5, books[1].Quantity);

            _unitOfWorkMock.Verify(uow => uow.OrderRepository.AddAsync(It.IsAny<Order>()), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.SaveChangesAsync(), Times.Once);
        }

        [Fact]
        public async Task CreateOrderAsync_WithInvalidQuantity_ShouldThrowException()
        {
            // Arrange
            var accountBalance = new AccountBalance(10000000);
            var customer = new Customer(Guid.NewGuid(), accountBalance.Id, "Customer", DateTime.UtcNow, "CustomerEmail");
            customer.AccountBalance = accountBalance;

            _unitOfWorkMock.Setup(uow => uow.CustomerRepository.GetByIdAsync(customer.Id)).ReturnsAsync(customer);

            var books = new List<Book>
            {
                new Book("Book 1", "Author 1", "Genre 1", DateTime.Now, "cover1.jpg", 1, 2),
                new Book("Book 2", "Author 2", "Genre 2", DateTime.Now, "cover2.jpg", 1, 5)
            };

            foreach (var book in books)
            {
                _unitOfWorkMock.Setup(uow => uow.BookRepository.GetByIdAsync(book.Id)).ReturnsAsync(book);
            }

            var orderDetailsForInsertDTO = new List<OrderDetailForInsertDTO>
                {
                    new OrderDetailForInsertDTO
                    {
                        BookId = books[0].Id,
                        UnitPrice = books[0].UnitPrice,
                        Quantity = 5
                    },
                    new OrderDetailForInsertDTO
                    {
                        BookId = books[1].Id,
                        UnitPrice = books[1].UnitPrice,
                        Quantity = 5
                    },
            };

            var orderForInsertDTO = new OrderForInsertDTO
            {
                CustomerId = customer.Id,
                OrderDetails = orderDetailsForInsertDTO
            };

            _unitOfWorkMock.Setup(uow => uow.OrderRepository.AddAsync(It.IsAny<Order>())).Returns(Task.CompletedTask);

            // Act
            // Assert
            var exception = await Assert.ThrowsAsync<Exception>(() => _orderService.CreateOrderAsync(orderForInsertDTO));
            Assert.Equal(ErrorMessages.InsufficientBookQuantity, exception.Message);
        }

        [Fact]
        public async Task CreateOrderAsync_WithInvalidTotal_ShouldThrowException()
        {
            // Arrange
            var accountBalance = new AccountBalance(10000000);
            var customer = new Customer(Guid.NewGuid(), accountBalance.Id, "Customer", DateTime.UtcNow, "CustomerEmail");
            customer.AccountBalance = accountBalance;

            _unitOfWorkMock.Setup(uow => uow.CustomerRepository.GetByIdAsync(customer.Id)).ReturnsAsync(customer);

            var books = new List<Book>
            {
                new Book("Book 1", "Author 1", "Genre 1", DateTime.Now, "cover1.jpg", 10, 10000000000000000000),
                new Book("Book 2", "Author 2", "Genre 2", DateTime.Now, "cover2.jpg", 10, 10000000000000000000)
            };

            foreach (var book in books)
            {
                _unitOfWorkMock.Setup(uow => uow.BookRepository.GetByIdAsync(book.Id)).ReturnsAsync(book);
            }

            var orderDetailsForInsertDTO = new List<OrderDetailForInsertDTO>
                {
                    new OrderDetailForInsertDTO
                    {
                        BookId = books[0].Id,
                        UnitPrice = books[0].UnitPrice,
                        Quantity = 10
                    },
                    new OrderDetailForInsertDTO
                    {
                        BookId = books[1].Id,
                        UnitPrice = books[1].UnitPrice,
                        Quantity = 10
                    },
            };

            var orderForInsertDTO = new OrderForInsertDTO
            {
                CustomerId = customer.Id,
                OrderDetails = orderDetailsForInsertDTO
            };

            _unitOfWorkMock.Setup(uow => uow.OrderRepository.AddAsync(It.IsAny<Order>())).Returns(Task.CompletedTask);

            // Act
            // Assert
            var exception = await Assert.ThrowsAsync<Exception>(() => _orderService.CreateOrderAsync(orderForInsertDTO));
            Assert.Equal(ErrorMessages.InsufficientBalance, exception.Message);
        }

        [Fact]
        public async Task CheckOutOrderAsync_ShouldCheckOutOrderAndSendEmail()
        {
            // Arrange
            var accountBalance = new AccountBalance(200);
            var customer = new Customer(Guid.NewGuid(), accountBalance.Id, "Customer", DateTime.UtcNow, "CustomerEmail");
            customer.AccountBalance = accountBalance;

            var order = new Order(customer.Id, DateTime.UtcNow);
            order.Customer = customer;
            order.CalculateTotal(10, 10);

            _unitOfWorkMock.Setup(uow => uow.OrderRepository.GetByIdAsync(order.Id)).ReturnsAsync(order);

            // Act
            await _orderService.CheckOutOrderAsync(order.Id);

            // Assert
            _unitOfWorkMock.Verify(uow => uow.OrderRepository.GetByIdAsync(order.Id), Times.Once);
            Assert.True(order.IsPurchase);
            Assert.Equal(100, order.Customer.AccountBalance.Balance);

            _emailSender.Verify(sender => sender.SendMailAsync(
                It.IsAny<string>(),
                It.IsAny<StringBuilder>(),
                It.IsAny<string[]>()),
                Times.Once);

            _unitOfWorkMock.Verify(uow => uow.SaveChangesAsync(), Times.Once);
        }

        [Fact]
        public async Task CheckOutOrderAsync_WithPurhcasedTrue_ShouldThrowException()
        {
            // Arrange
            var accountBalance = new AccountBalance(200);
            var customer = new Customer(Guid.NewGuid(), accountBalance.Id, "Customer", DateTime.UtcNow, "CustomerEmail");
            customer.AccountBalance = accountBalance;

            var order = new Order(customer.Id, DateTime.UtcNow);
            order.Customer = customer;
            order.CalculateTotal(10, 10);
            order.CheckOut();

            _unitOfWorkMock.Setup(uow => uow.OrderRepository.GetByIdAsync(order.Id)).ReturnsAsync(order);

            // Act
            // Assert
            var exception = await Assert.ThrowsAsync<Exception>(() => _orderService.CheckOutOrderAsync(order.Id));
            Assert.Equal(ErrorMessages.OrderAlreadyPurchased, exception.Message);
        }

        [Fact]
        public async Task GetById_ShouldCallGetByIdAsync()
        {
            // Arrange
            var order = new Order(It.IsAny<Guid>(), DateTime.UtcNow);
            _unitOfWorkMock.Setup(uow => uow.OrderRepository.GetByIdAsync(order.Id)).ReturnsAsync(order);

            // Act
            var result = await _orderService.GetByIdAsync(order.Id);

            // Assert
            _unitOfWorkMock.Verify(uow => uow.OrderRepository.GetByIdAsync(order.Id), Times.Once);
        }

        [Fact]
        public async Task GetByCustomerIdAsync_ShouldCallGetByCustomerIdAsync()
        {
            // Arrange
            var paginationDTO = new PaginationDTO
            {
                PageNumber = 1,
                PageSize = 10
            };

            var orders = new List<Order>()
            {
                new Order(It.IsAny<Guid>(), DateTime.UtcNow),
                new Order(It.IsAny<Guid>(), DateTime.UtcNow)
            };

            var ordersForGetDTO = new List<OrderForGetDTO>()
            {
                new OrderForGetDTO(),
                new OrderForGetDTO()
            };

            var paginatedListOrder = new PaginatedList<Order>(
                orders,
                orders.Count,
                paginationDTO.PageNumber,
                paginationDTO.PageSize);

            _unitOfWorkMock.Setup(
                uow => uow.OrderRepository.GetByCustomerIdAsync(It.IsAny<Guid>(),
                paginationDTO.PageNumber,
                paginationDTO.PageSize)).ReturnsAsync(paginatedListOrder);

            var mapperResult = new PaginatedList<OrderForGetDTO>(
                ordersForGetDTO,
                ordersForGetDTO.Count,
                paginationDTO.PageNumber,
                paginationDTO.PageSize);

            _mapperMock.Setup(m => m.Map<PaginatedList<OrderForGetDTO>>(paginatedListOrder)).Returns(mapperResult);

            // Act
            await _orderService.GetByCustomerIdAsync(It.IsAny<Guid>(), paginationDTO);

            //Assert
            _unitOfWorkMock.Verify(uow => uow.OrderRepository.GetByCustomerIdAsync(It.IsAny<Guid>(), It.IsAny<int>(), It.IsAny<int>()));
        }

        [Fact]
        public async Task GetOrderDetailsByOrderIdAsync_ShouldCallGetByOrderIdAsync()
        {
            // Arrange
            var orderId = Guid.NewGuid();

            var paginationDTO = new PaginationDTO
            {
                PageNumber = 1,
                PageSize = 10
            };

            var orderDetails = new List<OrderDetail>()
            {
                new OrderDetail(orderId, It.IsAny<Guid>(), 5, 5),
                new OrderDetail(orderId, It.IsAny<Guid>(), 5, 5),
            };

            var ordersDetailsForGetDTO = new List<OrderDetailForGetDTO>()
            {
                new OrderDetailForGetDTO(),
                new OrderDetailForGetDTO()
            };

            var paginatedListOrderDetail = new PaginatedList<OrderDetail>(
                orderDetails,
                orderDetails.Count,
                paginationDTO.PageNumber,
                paginationDTO.PageSize);

            _unitOfWorkMock.Setup(
                uow => uow.OrderDetailRepository.GetByOrderIdAsync(orderId,
                paginationDTO.PageNumber,
                paginationDTO.PageSize)).ReturnsAsync(paginatedListOrderDetail);

            var mapperResult = new PaginatedList<OrderDetailForGetDTO>(
                ordersDetailsForGetDTO,
                ordersDetailsForGetDTO.Count,
                paginationDTO.PageNumber,
                paginationDTO.PageSize);

            _mapperMock.Setup(m => m.Map<PaginatedList<OrderDetailForGetDTO>>(paginatedListOrderDetail)).Returns(mapperResult);

            // Act
            await _orderService.GetOrderDetailsByOrderIdAsync(orderId, paginationDTO);

            //Assert
            _unitOfWorkMock.Verify(uow => uow.OrderDetailRepository.GetByOrderIdAsync(orderId, It.IsAny<int>(), It.IsAny<int>()));
        }

        [Fact]
        public async Task GetPurchasedBooksByCustomerIdAsync_ShouldCallGetPurchasedBooksByCustomerIdAsync()
        {
            var customerId = Guid.NewGuid();

            // Arrange
            var paginatedListDTO = new PaginationDTO
            {
                PageNumber = 1,
                PageSize = 10
            };

            var books = new List<Book>
            {
                new Book("Book 1", "Author 1", "Genre 1", DateTime.Now, "cover1.jpg", 5, 9.99m),
                new Book("Book 2", "Author 2", "Genre 2", DateTime.Now, "cover2.jpg", 10, 19.99m)
            };

            var booksForGetDTO = new List<BookForGetDTO>
            {
                new BookForGetDTO(),
                new BookForGetDTO()
            };

            var paginatedListBook = new PaginatedList<Book>(
                books,
                books.Count,
                paginatedListDTO.PageNumber,
                paginatedListDTO.PageSize);

            _unitOfWorkMock.Setup(uow => uow.OrderRepository.GetPurchasedBooksByCustomerIdAsync(
                customerId,
                paginatedListDTO.PageNumber,
                paginatedListDTO.PageSize)).ReturnsAsync(paginatedListBook);

            var mapperResult = new PaginatedList<BookForGetDTO>(
                booksForGetDTO,
                books.Count,
                paginatedListDTO.PageNumber,
                paginatedListDTO.PageSize);

            _mapperMock.Setup(mapper => mapper.Map<PaginatedList<BookForGetDTO>>(paginatedListBook)).Returns(mapperResult);

            // Act
            var result = await _orderService.GetPurchasedBooksByCustomerIdAsync(customerId, paginatedListDTO);

            // Assert
            _unitOfWorkMock.Verify(uow => uow.OrderRepository.GetPurchasedBooksByCustomerIdAsync(It.IsAny<Guid>(), It.IsAny<int>(), It.IsAny<int>()), Times.Once);
        }
    }
}