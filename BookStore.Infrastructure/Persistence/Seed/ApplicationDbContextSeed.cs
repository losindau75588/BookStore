﻿using BookStore.Infrastructure.Commons.Constants;
using BookStore.Domain.Entities;
using BookStore.Infrastructure.Persistence.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using BookStore.Infrastructure.Commons.Helpers;
using BookStore.Infrastructure.Identity;

namespace BookStore.Infrastructure.Persistence.Seed
{
    public static class ApplicationDbContextSeed
    {
        public static async Task SeedAsync(
            ApplicationDbContext context,
            UserManager<ApplicationUser> userManager,
            RoleManager<IdentityRole> roleManager)
        {

            if (context.Database.IsSqlServer())
            {
                await context.Database.MigrateAsync();
            }

            try
            {
                await SeedIdentityAsync(context, userManager, roleManager);
                await SeedBooksAsync(context);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static async Task SeedIdentityAsync(
            ApplicationDbContext context,
            UserManager<ApplicationUser> userManager,
            RoleManager<IdentityRole> roleManager)
        {
            //Seed data role admin

            if (await roleManager.FindByNameAsync(IdentityRoles.Admin) == null)
            {
                var resultCreateRole = await roleManager.CreateAsync(new IdentityRole(IdentityRoles.Admin));

                if (!resultCreateRole.Succeeded)
                {
                    throw new Exception(ErrorMessages.CannotCreateRole);
                }
            }

            //Seed data role user

            if (await roleManager.FindByNameAsync(IdentityRoles.User) == null)
            {
                var resultCreateRole = await roleManager.CreateAsync(new IdentityRole(IdentityRoles.User));

                if (!resultCreateRole.Succeeded)
                {
                    throw new Exception(ErrorMessages.CannotCreateRole);
                }
            }

            //Seed data admin account

            if (await userManager.FindByEmailAsync("admin@gmail.com") == null)
            {
                var newAdmin = new ApplicationUser()
                {
                    Email = "admin@gmail.com",
                    UserName = "admin@gmail.com"
                };

                var resultCreateAdmin = await userManager.CreateAsync(newAdmin, "Admin@1234?");

                if (!resultCreateAdmin.Succeeded)
                {
                    throw new Exception(ErrorMessages.CannotCreateAppUser);
                }

                var adminFindByEmail = await userManager.FindByEmailAsync("admin@gmail.com");

                if (adminFindByEmail == null)
                {
                    throw new Exception(ErrorMessages.CannotFindAppUser);
                }

                var resultAddRole = await userManager.AddToRoleAsync(adminFindByEmail!, IdentityRoles.Admin);

                if (!resultAddRole.Succeeded)
                {
                    throw new Exception(ErrorMessages.CannotAddRole);
                }
            }

            //Seed data customer account

            if (await userManager.FindByEmailAsync("losindau75588@gmail.com") == null)
            {
                using (IDbContextTransaction transaction = await context.Database.BeginTransactionAsync())
                {
                    try
                    {
                        var newUser = new ApplicationUser()
                        {
                            Email = "losindau75588@gmail.com",
                            UserName = "losindau75588@gmail.com",
                            EmailConfirmed = true
                        };

                        var resultCreateUser = await userManager.CreateAsync(newUser, "User@1234?");

                        if (!resultCreateUser.Succeeded)
                        {
                            throw new Exception(ErrorMessages.CannotCreateAppUser);
                        }

                        var userFindByEmail = await userManager.FindByEmailAsync(newUser.Email);

                        if (userFindByEmail == null)
                        {
                            throw new Exception(ErrorMessages.CannotFindAppUser);
                        }

                        var resultAddRole = await userManager.AddToRoleAsync(userFindByEmail!, IdentityRoles.User);

                        if (!resultAddRole.Succeeded)
                        {
                            throw new Exception(ErrorMessages.CannotAddRole);
                        }

                        var accountBalance = new AccountBalance(1000000);
                        var customer = new Customer(new Guid(userFindByEmail.Id), accountBalance.Id, "Dau", DateTime.UtcNow, newUser.Email);
                        customer.AccountBalance = accountBalance;

                        await context.Customers.AddAsync(customer);
                        await context.SaveChangesAsync();
                        await transaction.CommitAsync();
                    }
                    catch (Exception)
                    {
                        await transaction.RollbackAsync();

                        throw;
                    }
                }
            }
        }

        public static async Task SeedBooksAsync(ApplicationDbContext context)
        {
            if (await context.Books.AnyAsync())
            {
                return;
            }

            string path = Path.Combine(JsonConstants.Folder, JsonConstants.BooksFileName);

            List<Book> books = JsonFileHelper.LoadJsonFile<Book>(path).ToList();

            await context.Books.AddRangeAsync(books);

            await context.SaveChangesAsync();
        }
    }
}