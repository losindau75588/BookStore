﻿using BookStore.Domain.Commons;
using BookStore.Domain.Entities;
using BookStore.Domain.Interfaces;
using BookStore.Infrastructure.Commons.Constants;
using BookStore.Infrastructure.Persistence.Data;
using Microsoft.EntityFrameworkCore;

namespace BookStore.Infrastructure.Persistence.Repositories
{
    public class ReviewRepository : Repository<Review>, IReviewRepository
    {
        public ReviewRepository(ApplicationDbContext context) : base(context)
        {
            
        }

        public async Task<PaginatedList<Review>> GetByCustomerIdAsync(
            int pageNumber,
            int pageSize,
            Guid customerId)
        {
            IQueryable<Review> query = _context.Reviews
                .Where(r => r.CustomerId == customerId)
                .Include(r => r.Book);

            int count = await query.CountAsync();

            List<Review> reviews = await query
                .OrderBy(b => b.Id)
                .Skip((pageNumber - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();

            if (reviews.Count < 1)
            {
                throw new Exception(ErrorMessages.EmptyPage);
            }

            return new PaginatedList<Review>(
                reviews,
                count,
                pageNumber,
                pageSize);
        }
    }
}