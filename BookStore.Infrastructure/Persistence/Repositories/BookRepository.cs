﻿using BookStore.Domain.Commons;
using BookStore.Domain.Entities;
using BookStore.Domain.Interfaces;
using BookStore.Infrastructure.Commons.Constants;
using BookStore.Infrastructure.Persistence.Data;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace BookStore.Infrastructure.Persistence.Repositories
{
    public class BookRepository : Repository<Book>, IBookRepository
    {
        public BookRepository(ApplicationDbContext context) : base(context)
        {
            
        }

        public async Task<PaginatedList<Book>> GetAllAsync(
            int pageNumber,
            int pageSize)
        {
            int count = await _context.Books.CountAsync();

            List<Book> books = await _context.Books
                .OrderBy(b => b.Id)
                .Skip((pageNumber - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();

            if (books.Count < 1)
            {
                throw new Exception(ErrorMessages.EmptyPage);
            }

            return new PaginatedList<Book>(
                books,
                count,
                pageNumber,
                pageSize);
        }

        public async Task<PaginatedList<Book>> FilterBooksAsync(
            int pageNumber,
            int pageSize,
            string? title,
            string? author,
            string? genre,
            DateTime? publicationDate)
        {
            Expression<Func<Book, bool>> expression = (book) => (string.IsNullOrEmpty(title) || book.Title.ToLower().Contains(title.ToLower().Trim())) &&
            (string.IsNullOrEmpty(author) || book.Author.ToLower().Contains(author.ToLower().Trim())) &&
            (string.IsNullOrEmpty(genre) || book.Genre.ToLower().Contains(genre.ToLower().Trim())) &&
            (!publicationDate.HasValue || book.PublicationDate.Date.Equals(publicationDate!.Value.Date));

            IQueryable<Book> query = _context.Books
                .Where(expression);

            int count = await query.CountAsync();

            List<Book> books = await query
                .OrderBy(b => b.Id)
                .Skip((pageNumber - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();

            if (books.Count < 1)
            {
                throw new Exception(ErrorMessages.EmptyPage);
            }

            return new PaginatedList<Book>(
                books,
                count,
                pageNumber,
                pageSize);
        }
    }
}