﻿namespace BookStore.Infrastructure.Services.Email
{
    public class EmailSettings
    {
        public const string SectionName = "EmailSettings";

        public int Port { get; set; }

        public bool EnableSsl { get; set; }

        public string Host { get; set; } = string.Empty;

        public bool UseDefaultCredentials { get; set; }

        public string EmailForSend { get; set; } = string.Empty;

        public string AppPassword { get; set; } = string.Empty;
    }
}