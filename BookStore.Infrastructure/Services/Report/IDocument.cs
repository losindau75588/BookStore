﻿using QuestPDF.Infrastructure;

namespace BookStore.Infrastructure.Services.Report
{
    public interface IDocument
    {
        DocumentMetadata GetMetadata();

        DocumentSettings GetSettings();

        void Compose(IDocumentContainer container);
    }
}
