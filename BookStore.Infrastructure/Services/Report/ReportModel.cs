﻿namespace BookStore.Infrastructure.Services.Report
{
    public class ReportModel
    {
        public int CountOrder { get; set; }
        public int CountPurchasedOrder { get; set; }
        public decimal Revenue { get; set; }
        public int CountNewUsers { get; set; }
    }
}
