﻿namespace BookStore.Infrastructure.Services.Report
{
    public class ReportSettings
    {
        public const string SectionName = "ReportSettings";

        public string Email { get; set; } = default!;
    }
}
