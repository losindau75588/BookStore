﻿using Microsoft.AspNetCore.Identity;

namespace BookStore.Infrastructure.Identity
{
    public class ApplicationUser : IdentityUser
    {

    }
}
